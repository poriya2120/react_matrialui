import { MuiThemeProvider } from 'material-ui/styles';
import React from 'react';
import './App.css';
import NavBar from './components/navbar/NavBar';
import Search from './components/serch/Search';

function App() {
  return (
    <MuiThemeProvider>
      <div>
      <NavBar/>
      <Search />

      </div>
      
    </MuiThemeProvider>
  );
}

export default App;
